package fr.afpa.objets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;

public class Chambre {

	private int numChambre;
	private String typeChambre;
	private String taille;
	private String vue;
	private String occupation;
	private String tarif;
	private String nombre;
	private String options;
	private Boolean chambreOccup = false;
	Reservation[] reservation;

	// Constructeurs
	public Chambre() {

	}

	public Chambre(int i, String type, String tailleChambre, String vueChambre, String occupationChambre,
			String tarifChambre, String nombreChambre, String OptionsChambre, Boolean strings) {

		numChambre = i;
		typeChambre = type;
		taille = tailleChambre;
		vue = vueChambre;
		occupation = occupationChambre;
		tarif = tarifChambre;
		nombre = nombreChambre;
		options = OptionsChambre;
		chambreOccup = strings;
	}

	public Chambre(int num, String[] temp, Boolean occ) {

		numChambre = num;
		typeChambre = temp[0];
		taille = temp[1];
		vue = temp[2];
		occupation = temp[3];
		tarif = temp[4];
		nombre = temp[5];
		options = temp[6];
		chambreOccup = occ;
	}

	public Chambre(String string, String string2, String string3, String string4, String string5, boolean b,
			Object object) {
		// TODO Auto-generated constructor stub
	}

	// getter
	public int getNumChambre() {
		return numChambre;
	}

	public String getTypeChambre() {
		return typeChambre;
	}

	public String getTaille() {
		return taille;
	}

	public String getVue() {
		return vue;
	}

	public String getOccupation() {
		return occupation;
	}

	public String getTarif() {
		return tarif;
	}

	public String getNombre() {
		return nombre;
	}

	public String getOptions() {
		return options;
	}

	public boolean getChambreOccup() {
		return chambreOccup;
	}

	// Setter
	public void setTypeChambre(int numChambre_) {
		numChambre = numChambre_;
	}

	public void setTypeChambre(String type_) {
		typeChambre = type_;
	}

	public void setTaille(String taille_) {
		taille = taille_;
	}

	public void setOccupation(String occ) {
		occupation = occ;
	}

	public void setTarif(String tarif_) {
		tarif = tarif_;
	}

	public void setNombre(String nombre_) {
		nombre = nombre_;
	}

	public void setOptions(String options_) {
		options = options_;
	}

	public void setChambreOccup(Boolean occ) {
		chambreOccup = occ;
	}

	public void genererChambre() {

		String[] chambre = null ;        
		
			try {
            FileReader lecteur = new FileReader("Fichiers\\ListeChambres_V3.csv");
            int chiffreAffaire=0;
            String descriptionChambre = "";
            BufferedReader abc= new BufferedReader(lecteur);
			if (abc.ready()) {
                chambre = abc.readLine().split(";");
            }
            int sommeChambre=0;
			while (abc.ready()) {
                chambre = abc.readLine().split(";");                
               sommeChambre += Integer.parseInt(chambre[5]);
                
            }
            System.out.println("il y a "+sommeChambre+"chambres");
            System.out.println();            
            abc.close();            
            
            Chambre[] listeChambre = new Chambre[sommeChambre];
            int indiceChambre = 0;            
            FileReader lecteur2 = new FileReader("Fichiers\\ListeChambres_V3.csv");            
            BufferedReader abc2 = new BufferedReader(lecteur2);
            if (abc2.ready()) {
                chambre = abc2.readLine().split(";");
            }
            while (abc2.ready()) {
                chambre = abc2.readLine().split(";");
            
            for (int i = 0; i < Integer.parseInt(chambre[5]); i++) {
                
                
                
                listeChambre[indiceChambre] = new Chambre(chambre[0], chambre[1], chambre[2], chambre[4], chambre[6],true, null );
                        
                indiceChambre++;
            }}
            for (int i = 0; i < listeChambre.length; i++) {
               // System.out.println(i+1 + " - "  + listeChambre[i]);
                
            }
            abc2.close();                    }        catch (Exception e) {
            System.out.println("Erreur" + e);
        }
    }

	@Override
	public String toString() {
		return "Chambre [numChambre=" + numChambre + ", typeChambre=" + typeChambre + ", taille=" + taille + ", vue="
				+ vue + ", occupation=" + occupation + ", tarif=" + tarif + ", nombre=" + nombre + ", options="
				+ options + ", chambreOccup=" + chambreOccup + ", reservation=" + Arrays.toString(reservation) + "]";
	}



		
	




		
	



		
	

	}
	

	

	
			
			/*csv.close();// Fermeture du flux
		} catch (Exception f) {
			System.out.println("erreur" + f);
		} finally {
			if (csv != null) {
				try {
					csv.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return chambre;
	}

	@Override
	public String toString() {
		return "Chambre [numChambre=" + numChambre + ", typeChambre=" + typeChambre + ", taille=" + taille + ", vue="
				+ vue + ", occupation=" + occupation + ", tarif=" + tarif + ", nombre=" + nombre + ", options="
				+ options + ", chambreOccup=" + chambreOccup + ", reservation=" + Arrays.toString(reservation) + "]";
	}

}*/
