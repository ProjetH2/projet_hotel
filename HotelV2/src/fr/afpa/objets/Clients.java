package fr.afpa.objets;

import java.time.LocalDate;
import java.util.Scanner;

public class Clients {
	private String nomClient;
	private String prenomClient;
	private String dateDeNaissanceClient;
	private String emailClient;
	private String tel;
	private String identifiantClient;

//Constructeur(s)
	public Clients(String nom, String prenom, String dateNaissance, String email, String tel_, String id) {

		nomClient = nom;
		prenomClient = prenom;
		dateDeNaissanceClient = dateNaissance;
		emailClient = email;
		tel = tel_;
		identifiantClient = id;
	}

	public Clients() {

	}

	// Getter
	public String getNomClient() {
		return nomClient;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public String getDateDeNaissanceClient() {
		return dateDeNaissanceClient;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public String getTel() {
		return tel;
	}

	public String getIdentifiantClient() {
		return identifiantClient;
	}

	// Setter

	public void setNomClient(String nom) {
		nomClient = nom;
	}

	public void setPrenomClient(String prenom) {
		prenomClient = prenom;
	}

	public void setDateDeNaissanceClient(String dateNaissance) {
		dateDeNaissanceClient = dateNaissance;
	}

	public void setEmailClient(String email) {
		emailClient = email;
	}

	public void setTel(String tel_) {
		tel = tel_;
	}

	public void setIdentifiantClient(String id) {
		identifiantClient = id;
	}

	@Override
	public String toString() {
		return "Clients [nomClient=" + nomClient + ", prenomClient=" + prenomClient + ", dateDeNaissanceClient="
				+ dateDeNaissanceClient + ", emailClient=" + emailClient + ", tel=" + tel + ", identifiantClient="
				+ identifiantClient + "]";
	}

}
