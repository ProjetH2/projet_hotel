package fr.afpa.objets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Scanner;

public class Hotel {

	Chambre[] chambre;
	Clients[] client;
	String idClient;
	String idEmploye = "";
	String mdp = "";
	Reservation[] Reservation;
	char c;

	boolean quitter = false;
	Scanner in = new Scanner(System.in);

	public void lancementProgramme() {
		new Chambre().genererChambre();
		//System.out.println(Arrays.toString(chambre.genererChambre()));
		choixConnexion(idEmploye, idClient);
		while (!quitter) {
			quitter = choixMenu(c, mdp, chambre, quitter);
		}
	}

	// Méthode pour l'affichage du menu de l'hotel

	public void afficherMenu() {
		System.out.println(
				"-------------------------------   MENU HOTEL CDA JAVA   ----------------------------------------------");
		System.out.println();
		System.out.println("A- Afficher l’état de l’hôtel");
		System.out.println("B- Afficher le nombre de chambres réservées");
		System.out.println("C- Afficher le nombre de chambres libres");
		System.out.println("D- Afficher le numéro de la première chambre vide");
		System.out.println("E- Afficher le numéro de la dernière chambre vide");
		System.out.println("F- Réserver une chambre ");
		System.out.println("G- Libérer une chambre ");
		System.out.println("H- Modifier une réservation");
		System.out.println("I- Annule une réservation");
		System.out.println("J- Afficher le chiffre d'affaire");
		System.out.println("Q- Quitter");
		System.out.println(
				"---------------------------------------------------------------------------------------------------------");
		System.out.println();
		System.out.println("Votre choix:");

	}

	// Méthode pour la sélection du menu
	public boolean choixMenu(char c, String mdp, Chambre[] chambre, boolean quitter) {
		Scanner in = new Scanner(System.in);
		char choix = in.next().charAt(0);
		in.nextLine();
		switch (choix) {
		case 'a':
		case 'A':
			etatHotel(chambre);
			break;
		case 'b':
		case 'B':
			chambreReserver(chambre);
			break;
		case 'c':
		case 'C':
			chambreLibre(chambre);
			break;
		case 'd':
		case 'D':
			premiereLibre(chambre);
			break;
		case 'e':
		case 'E':
			derniereLibre(chambre);
			break;
		case 'f':
		case 'F':
			new Reservation().reservation(chambre);

		case 'g':
		case 'G':
			System.out.println("En cours de développement");
			break;
		case 'h':
		case 'H':
			System.out.println("En cours de développement");
			break;
		case 'i':
		case 'I':
			System.out.println("En cours de développement");
			break;
		case 'j':
		case 'J':
			System.out.println("En cours de développement");
			break;
		case 'q':
		case 'Q':
			System.out.println("Aurevoir");
			choixConnexion(idEmploye, idClient);
			return true;
		default:
			System.out.println("Erreur");
			break;
		}in.close();
		return false;
	}

	// choix de la personne qui souhaite se loger (Client ou Employer)
	public void choixConnexion(String idEmployer, String idClient) {
		boolean verifChoix = false;
		while (!verifChoix) {
			System.out.println("Client ou employer (C ou E) ?"); // mettre la demande de saisis de l'utilisateur dans la
																	// boucle, sinon on se retrouve avec une boucle
																	// infini
			String choix = in.nextLine();
			// Contrôle de la saisis de l'utilisateur uniquement 1 caractère et qu'il soit
			// un 'C' ou un 'E'
			if (choix.length() == 1 && choix.charAt(0) == 'C') {
				connexionClient(idClient);
				verifChoix = true;
				break;// on break pour sortir de la boucle
			} else if (choix.length() == 1 && choix.charAt(0) == 'E') {
				connexionEmployer(idEmploye, mdp);
				verifChoix = true;
				break;
			} else if (choix.length() == 1 && (choix.charAt(0) != 'C' || choix.charAt(0) != 'E')
					|| choix.length() != 1) {
				System.out.println("Seul un employer ou un client peuvent se connecter");

			}
		}
	}

	// Login employer
	public void connexionEmployer(String idEmploye, String mdp) {
		Scanner in = new Scanner(System.in);
		boolean verifIdEmploye = false;
		String mdpVerif = "";

		// contrôle de la complexité de l'identifiant employer (Doit faire 5 de longueur
		// avec les 2 1er caractères "GH" suivi de 3 chiffres

		while (!verifIdEmploye) {
			System.out.println("Identifiant : ");
			String id = in.nextLine();
			if (id.length() == 5) {
				// condition pour detecter que les 2 1er caractère soit un 'G' et un 'H'
				if (id.charAt(0) == 'G' && id.charAt(1) == 'H') {
					// Boucle pour contrôler le reste de la chaîne de caractère et que ceux si sont
					// des digit
					for (int i = 2; i < 6; i++) {
						if ((Character.isDigit(id.charAt(i)))) {
							verifIdEmploye = true;
							break;
						} else {
							System.out.println("Login employé inconnu");
							verifIdEmploye = true;

						}
					}
				}
			} else {
				System.out.println("Le Login doit avoir une longueur de 5");
			}
		}
		// vérification du mot de passe employer
		System.out.println("Mot de passe : ");
		mdp = in.nextLine();
		//boolean verifMdpEmploye = false;
		// Lecture du fichier contenant le mot de passe
		try {
			FileReader fr = new FileReader("Fichiers\\password.txt");// lecture
																								// du
			// mdp
			// employé
			// dans le
			// fichier
			// txt
			BufferedReader br = new BufferedReader(fr);
			while (br.ready())
				// Mettre le mot de passe dans une variable afin de faire la comparaison
				mdpVerif = br.readLine();
			System.out.println(mdpVerif);
			br.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
		// Comparaison du mot de passe saisi par l'employer avec celui contenu dans le
		// fichier
		if (mdp.contentEquals(mdpVerif))
			afficherMenu();

		else {
			System.out.println("Mot de passe incorrect");
		}
	}
	// login client

	public void connexionClient(String idClient) {
		boolean verifLogClient = false;
		// Contrôle de la longueur de l'identifiant du client (10 Chiffres)
		while (!verifLogClient) {
			System.out.println("Identifiant : ");
			String LogClient = in.nextLine();
			if (LogClient.length() == 10) {
				// Boucle de vérification que le login ne contienne que des digits
				for (int i = 0; i < 10; i++) {
					if ((Character.isDigit(LogClient.charAt(i)))) {
						verifLogClient = true;
						break;

					} else {
						System.out.println("Login client inconnu");
						verifLogClient = true;

					}
				}
			} else {
				System.out.println("Le Login doit contenir 10 chiffres");
			}

		}
		System.out.println("Affichage de votre réservation: ");// afficher la réservation du client
		try {
			Thread.sleep(20000);// Affichage pendant 20 secondes
			choixConnexion(idEmploye, idClient);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Affichage de l'état de l'hotel (Affichage de toutes les chambres)
	public void etatHotel(Chambre[] chambre) {
		for (int i = 0; i < chambre.length; i++) {
			System.out.println(chambre[i].toString());

		}
	}

	// Affichage de toutes les chambres qui sont réservées
	public void chambreReserver(Chambre[] chambre) {
		int comptReserver = 0;
		for (int i = 0; i < chambre.length; i++) {
			if (chambre[i].getChambreOccup() == true) {
				comptReserver += 1;
			}
			System.out.println("Il y a " + comptReserver + " chambre de réserver");
		}
	}

	// Affichage de toutes les chambres Libres
	public void chambreLibre(Chambre[] chambre) {
		int comptLibre = 0;
		for (int i = 0; i < chambre.length; i++) {
			if (chambre[i].getChambreOccup() == true) {
				comptLibre++;
			}
			System.out.println("Il y a " + comptLibre + " chambre de réserver");
		}
	}

	// affichage de la dernière chambre libre
	public void derniereLibre(Chambre[] chambre) {
		for (int i = chambre.length; i > chambre.length - 1; i--) {
			if (chambre[i].getChambreOccup() == false) {
				break;
			}
			System.out.println(chambre[i].toString());
		}

	}

	// Affichage de la première chambre libre
	public void premiereLibre(Chambre[] chambre) {
		for (int i = 0; i < chambre.length - 1; i++) {
			if (chambre[i].getChambreOccup() == false) {
				break;
			}
			System.out.println(chambre[i].toString());
		}

	}

	// Calcul du chiffre d'affaire
	public void chiffreAffaire() {

	}

	@Override
	public String toString() {
		return "Hotel [chambre=" + Arrays.toString(chambre) + ", client=" + Arrays.toString(client) + ", idClient="
				+ idClient + ", idEmploye=" + idEmploye + ", mdp=" + mdp + ", Reservation="
				+ Arrays.toString(Reservation) + ", c=" + c + ", quitter=" + quitter + ", in=" + in + "]";
	}
}
