package fr.afpa.objets;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DoubleBorder;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

public class Reservation {

	private String dateDebut;
	private String dateFin;
	Clients[] clients;
	Clients client = new Clients();
	Chambre[] chambre;
	Chambre chambres = new Chambre();
	
	// Constructeurs

	public Reservation(String dateDeb, String dateF, Clients client_) {

		dateDebut = dateDeb;
		dateFin = dateF;
		client = client_;
	}

	public Reservation() {

	}

	// Getter
	public String getDateDebut() {
		return dateDebut;
	}

	public String getDateFin() {
		return dateFin;
	}

	// Setter

	public void setDateDebut(String dateDeb) {
		dateDebut = dateDeb;
	}

	public void setDateFin(String dateF) {
		dateFin = dateF;
	}

	public void reservation(Chambre[] chambres) {
		Scanner in = new Scanner(System.in);
		//Chambre chambre = new Chambre();
		//System.out.println(Arrays.toString(chambre.genererChambre()));
		System.out.println("Mot de passe : ");
		String mdp = in.nextLine();
		String mdpVerif = null;
		boolean verifMdpEmploye = false;
		// Lecture du fichier contenant le mot de passe
		try {
			FileReader fr = new FileReader("Fichiers\\password.txt");// lecture
																								// du
			// mdp
			// employé
			// dans le
			// fichier
			// txt
			BufferedReader br = new BufferedReader(fr);
		
			while (br.ready())
				// Mettre le mot de passe dans une variable afin de faire la comparaison
				mdpVerif = br.readLine();
			br.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
		// Comparaison du mot de passe saisi par l'employer avec celui contenu dans le
		// fichier
		if (mdp.contentEquals(mdpVerif)) {
			

		

		
		boolean dateD = false;
		boolean dateF = false;
		LocalDate date1 = null;
		LocalDate date2 = null;

		while (!dateD && !dateF) {
			try {

				System.out.println("Veuillez entrer la date de debut du séjour : JJ/MM/AAAA");
				String dateDebut = in.nextLine();
				System.out.println("Veuillez entrer la date de fin du séjour : JJ/MM/AAAA");
				String dateFin = in.nextLine();

				String[] checkDate1 = dateDebut.split("/");
				String[] checkDate2 = dateFin.split("/");

				date1 = LocalDate.parse(checkDate1[2] + "-" + checkDate1[1] + "-" + checkDate1[0]);
				date2 = LocalDate.parse(checkDate2[2] + "-" + checkDate2[1] + "-" + checkDate2[0]);

				break; // si la date est correcte, boolean a true et break pour sortir de la boucle
			} catch (Exception e) {
				System.out.println("La date n'est pas valide.Veuillez bien respecter la saisie de la date JJ/MM/AAAA");
			}

		}

		if (date1.isAfter(date2) == true) {
			System.out.println("ERREUR DATE, la date de fin doit être ultérieure à la date de debut du sejour");
		}

		if (date1.isBefore(LocalDate.now()) == true) {
			System.out.println("ERREUR DATE, pas de reservation possible avant aujourd'hui");
		}

		for (int i = 0; i <66; i++) {
			if (chambre[i].getChambreOccup() == false) {
				
				System.out.println(chambre[i]);
			}

		}
		boolean verifNum = false;// verification de de la saisie du numéro de chambre a reserver
		int selectionChambre = 0;
		while (!verifNum) {
			System.out.println("Selectionner le numero de la chambre à reserver");
			selectionChambre = in.nextInt();
			for (int i = 0; i < chambre.length; i++) {

				if (selectionChambre == chambre[i].getNumChambre()
						&& (chambre[i].getChambreOccup() == false)) {
					verifNum = true;
					break;
				} else {
					System.out.println("Numero de chambre inconnu ou indisponible");
					verifNum = false;
				}
			}

		}

		System.out.println("Veuillez saisir le nom du client");
		String nomCli = in.nextLine();
		System.out.println("Veuillez saisir le prénom du client");
		String prenomcli = in.nextLine();
		String email = verificationMail();// fonction verif email client
		System.out.println("Veuillez saisir le numéro de téléphone du client");
		String numCli = in.nextLine();
		String dateNaiss = dateDeNaissanceClient();// fonction verif date de naissance valide

		Clients client = new Clients();

		client.setNomClient(nomCli);
		client.setPrenomClient(prenomcli);
		client.setEmailClient(email);
		client.setTel(numCli);
		client.setDateDeNaissanceClient(dateNaiss);

		// affichage des information de la réservation qui vient d'être faite
		System.out.println("Vous avez choisit:" + selectionChambre + " "
				+ chambre[selectionChambre].getTypeChambre() + "\n" + "Du: " + dateDebut + " Au: " + dateFin);
		System.out.println("Nom: " + client.getNomClient() + " prenom: " + client.getPrenomClient() + "Email : "
				+ client.getEmailClient() + "Tel : " + client.getTel());
		// Affichage du montant de la réservation
		int dureeSejour = Period.between(date1, date2).getDays();
		int total = dureeSejour * Integer.parseInt(chambre[selectionChambre].getTarif());
		System.out.println("Le total de vôtre séjour est de : " + total);
		// demande du paiement
		boolean controlCB = false;
		while (!controlCB) {
			System.out.println("Veuillez entrer le numéro de carte bancaire (Contient 10 chiffres): ");
			String cb = in.nextLine();
			if (cb.length() == 10) {
				for (int i = 0; i < 10; i++) {
					if (Character.isDigit(cb.charAt(i))) {
						controlCB = true;
						break;
					} else {
						System.out.println("Le code de carte bancaire doit contenir que des chiffres");
						controlCB = true;
					}
				}
			} else {
				System.out.println("Le code doit contenir 10 chiffres");
			}

		}
		
		in.close();
		generationPDF(total, date1, date2, dureeSejour);
		}
	}

	// Génération pdf

	public void generationPDF(int total, LocalDate date1, LocalDate date2, int dureeSejour) {
		String cheminPDF = "Fichiers\\Reservation.pdf";
		Document document = null;
		// Création du fichier PDF
		try {
			PdfWriter writer = new PdfWriter(cheminPDF);
			PdfDocument pdf = new PdfDocument(writer);
			document = new Document(pdf);
		} catch (FileNotFoundException a) {
			System.out.println("erreur");
		}

		try {
			// Information de la réservation
			document.add(new Paragraph("Facture HOTEL CDA-JAVA")).setTextAlignment(TextAlignment.CENTER).setFontSize(15)
					.setBold();
			document.add(new Paragraph(" "));
			Paragraph infoClient = new Paragraph();
			infoClient.add("Nom: " + client.getNomClient() + "\n").setFontSize(10).setWidth(500);
			infoClient.add("Prénom: " + client.getPrenomClient() + "\n").setFontSize(10).setWidth(500);
			infoClient.add("Date de naissance: " + client.getDateDeNaissanceClient() + "\n").setFontSize(10)
					.setWidth(500);
			infoClient.add("Email: " + client.getEmailClient() + "\n").setFontSize(10).setWidth(500);
			infoClient.add("N° de réservation: 1025968763\n").setTextAlignment(TextAlignment.LEFT).setFontSize(10)
					.setWidth(500);
			infoClient.add("Identifiant : 0123456789").setFontSize(10).setWidth(500);
			infoClient.add("Fait le :" + LocalDate.now()).setFontSize(10).setWidth(500);
			document.add(infoClient);

			// tableau récapitulatif de la réservation (Chambre réserver+ facture)
			Table facture = new Table(2);
			facture.setFixedPosition(50, 550, 500);
			facture.setBorder(new DoubleBorder(3));

			Cell cell = new Cell().add(new Paragraph("Récapitulatif de la réservation"))
					.setBorderBottom(new SolidBorder(1.5f)).setTextAlignment(TextAlignment.CENTER).setBold()
					.setFontSize(11).setBackgroundColor(ColorConstants.GRAY);
			facture.addCell(cell);
			cell = new Cell().add(new Paragraph("Prix")).setBorderBottom(new SolidBorder(1.5f))
					.setBackgroundColor(ColorConstants.GRAY).setTextAlignment(TextAlignment.CENTER).setBold()
					.setFontSize(11);
			facture.addCell(cell);
			cell = new Cell()
					.add(new Paragraph(chambres.getTypeChambre() + "\n" + "Du: " + date1 + " Au: " + date2 + "\n"
							+ "nombre de jour/s: " + dureeSejour))
					.setTextAlignment(TextAlignment.LEFT).setBold().setFontSize(11);
			facture.addCell(cell);
			cell = new Cell().add(new Paragraph(chambres.getTarif()).setTextAlignment(TextAlignment.CENTER).setBold()
					.setFontSize(11));
			facture.addCell(cell);
			cell = new Cell(1, 2).add(new Paragraph("Total : " + total)).setBorder(Border.NO_BORDER)
					.setTextAlignment(TextAlignment.RIGHT).setBold().setFontSize(11);
			facture.addCell(cell);
			document.add(facture);
		} catch (Exception e) {
			System.out.println("erreur" + e);
		} finally {
			document.close();
		}
		envoiMail();
	}

	// Envoi de l'email

	public void envoiMail() {
		String user = "nootator.noot@gmail.com";
		String pass = "Noot_noot59";
		String serveur = "smtp.gmail.com";

		// Propriétés

		Properties props = System.getProperties();
		props.put("mail.smtp.host", serveur);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", 587);
		props.put("mail.smtp.auth", "true");
		Session session = Session.getInstance(props);

		try {
			// Création du message
			Message message = new MimeMessage(session);

			// Expéditeur
			message.setFrom(new InternetAddress(user));

			// Destinataire(s)
			InternetAddress[] internetAdresses = new InternetAddress[1];
			internetAdresses[0] = new InternetAddress(client.getEmailClient());
			message.setRecipients(Message.RecipientType.TO, internetAdresses);

			// Objet du message
			message.setSubject("Récapitulatif de votre réservation");

			// Zone texte du mail
			MimeBodyPart body = new MimeBodyPart();
			body.setText(
					"Merci de vôtre réservation a l'hôtel CDA JAVA \n voici le récapitulatif ainsi que vôtre facture.");

			// Pièce jointe fichier
			MimeBodyPart attachMent1 = new MimeBodyPart();
			String fichier = "Fichiers\\Reservation.pdf";
			attachMent1.attachFile(fichier);

			// Relie les parties entre elles
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(body);
			multipart.addBodyPart(attachMent1);
			message.setContent(multipart);

			// Permet d'envoyer le message
			Transport transport = session.getTransport("smtp");
			transport.connect(serveur, user, pass);
			transport.sendMessage(message, internetAdresses);
			transport.close();

		} catch (AddressException e) {
			System.out.println("Erreur" + e);
		} catch (MessagingException f) {
			System.out.println("Erreur" + f);
		} catch (Exception g) {
			System.out.println("erreur" + g);
		}
	}

	public void modificationReservation() {
		Scanner res = new Scanner(System.in);
		boolean verifMdp = false;
		while (!verifMdp) {
			System.out.println("Saisir mot de passe: ");
			String pass = res.nextLine();
			String mdpVerif = "";
			try {
				FileReader password = new FileReader("Fichiers\\password.txt");
				BufferedReader psw = new BufferedReader(password);

				while (psw.ready())
					mdpVerif = psw.readLine();
				psw.close();
			} catch (Exception e) {
				System.out.println("erreur" + e);
			}
		}
	}

	static String dateDeNaissanceClient() {

		Scanner in = new Scanner(System.in);
		boolean dateN = false;
		String dateNaissance = "";
		// boucle tant que la date n'est pas correct, demander la date.
		while (!dateN) {
			System.out.println("date de naissance client : JJ/MM/AAAA ");
			String naissanceCli = in.nextLine();
			in.nextLine();

			String[] donneesDate = naissanceCli.split("/");

			try {
				LocalDate date = LocalDate
						.parse(donneesDate[2] + "-" + LocalDate.parse(donneesDate[2] + "-" + donneesDate[0]));
				dateN = true;
				dateNaissance = date.toString();
				break; // si la date est correcte, boolean a true et break pour sortir de la boucle
			} catch (Exception e) {
				System.out.println("La date n'est pas valide.");
				dateN = false;
			}
		}
		in.close();
		return dateNaissance;
	}

	static String verificationMail() {// verification de l'adresse email client
		Scanner in = new Scanner(System.in);

		int compteur = 0;// compteur des arobases (un mail ne contient qu'un seul arobase)
		System.out.println("Veuillez saisir l'email  du client");
		String emailCli = in.nextLine();
		for (int i = 0; i < emailCli.length(); i++) {

			if (emailCli.charAt(i) == '@') {
				compteur++;// boucle pour comptabiliser les arobases
			}
			if (compteur != 1) {
				System.out.println("Email non valide;veuillez saisir un mail valide");
				break;

			} else {
				if (emailCli.charAt(0) != ('@') && compteur == 1 && emailCli.charAt(emailCli.length() - 4) == '.'
						|| (emailCli.charAt(emailCli.length() - 3) == '.')) {// verif de l'arobase et du .com ou .fr
				} else {
					System.out.println("Email non valide;veuillez saisir un mail valide");
				}
			}
		}
		return emailCli;
	}

	@Override
	public String toString() {
		return "Reservation [dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", clients=" + Arrays.toString(clients)
				+ ", client=" + client + ", chambre=" + Arrays.toString(chambre) + ", chambres=" + chambres + "]";
	}
}

/*
 * public boolean verificationDuree() { boolean verificationDuree=false;
 * 
 * while verficationDuree==false{ if
 * (dateDebut.isAfter(res.getDateFinResaX())==true &&
 * dateFin.isBefore(res.getDateDebutResaX())==true) {verificationDuree=true;}
 * //Contrôle de la date si la demande de réservation se situe entre 2 autres
 * réservations, boolean passe a true
 * 
 * 
 * else { if (dateDebut.isAfter(dateFinResaX)==false){ System.out.
 * println("Cette chambre n'est pas disponible a cette periode, veuillez modifier la date de debut du sejour"
 * ); } else (dateFin.isBefore(dateDebutResaX)==false){ System.out.
 * println("Cette chambre n'est pas disponible a cette periode, veuillez modifier la date de fin du sejour"
 * );
 * 
 * } if ((dateDebut==res.getDateFinResaX &&
 * dateFin.isBefore(res.getDateDebutResaX)||(dateFin==res.getDateDebutResaX&&
 * dateDebut.isAfter(res.getDateFinResaX) { verificationDuree=true; } }
 * 
 * /* public boolean verificationDuree() { boolean verificationDuree=false;
 * 
 * if (dateDebut.isAfter(res.getDateFinResaX())==true &&
 * dateFin.isBefore(res.getDateDebutResaX())==true) {verificationDuree=true;}
 * //Contrôle de la date si la demande de réservation se situe entre 2 autres
 * réservations, boolean passe a true
 * 
 * 
 * else { if (dateDebut.isAfter(dateFinResaX)==false){ System.out.
 * println("Cette chambre n'est pas disponible a cette periode, veuillez modifier la date de debut du sejour"
 * ); } else (dateFin.isBefore(dateDebutResaX)==false){ System.out.
 * println("Cette chambre n'est pas disponible a cette periode, veuillez modifier la date de fin du sejour"
 * );
 * 
 * } if ((dateDebut==res.getDateFinResaX &&
 * dateFin.isBefore(res.getDateDebutResaX)||(dateFin==res.getDateDebutResaX&&
 * dateDebut.isAfter(res.getDateFinResaX) { verificationDuree=true; } }
 * 
 * return verificationDuree;
 * 
 * }
 */
